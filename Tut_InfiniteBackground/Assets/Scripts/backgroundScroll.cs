﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class backgroundScroll : MonoBehaviour {

    Material material;
    Vector2 offset; //stores how much we want to offset in x & y direction

    public int xVelocity, yVelocity; //2 vars to store x/y numbers

    private void Awake()
    {
        material = GetComponent<Renderer>().material; //accesses the material component and stores it in a var
    }
    // Use this for initialization
    void Start () {
        offset = new Vector2(xVelocity, yVelocity); //the offset is a new position that has the vars xVel, yVel
	}
	
	// Update is called once per frame
	void Update () {
        material.mainTextureOffset += offset * Time.deltaTime; //movse backgrounds
	}
}
